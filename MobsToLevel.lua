local MobsToLevel_EventFrame = CreateFrame("Frame")
MobsToLevel_EventFrame:RegisterEvent("CHAT_MSG_COMBAT_XP_GAIN")
MobsToLevel_EventFrame:SetScript("OnEvent",
	function(self, event, ...)
		local arg1 = ...
	for creatureName, xp in string.gmatch(arg1, "(.+) dies, you gain (%d+) experience.") do 

			--how much xp do we need in total for this level?
			xpTotal = UnitXPMax("player")

			--how much rested xp do we have?
			xpRested = GetXPExhaustion()

			--how much xp have we gained for this level?
			xpCurrent = UnitXP("player") + xp

			--how much xp do we need to level up?
			xpToLevel = xpTotal - xpCurrent

			--if we have no rested xp
			--or we have rested xp for the whole level and beyond
			--or the rested xp is less than what you would get for killing a mob
			
			if xpRested == nil or xpRested >= xpToLevel then
			
				--we don't need to do fancy math to take into account rested xp
				--just divide your last xp gain, with the total you need to level.
				mobsToLevel = abs(xpToLevel / xp) + 1

			else
			
				--we have rested xp for a partial level, so we need to calculate how
				--many mobs we need to kill while we have rested xp, and then how
				--many we need when rested xp runs out, and add those numbers together
				
				--While you are rested you gain double xp for mobs, so we take the amount
				--of xp you just got, and divide your rested xp with that. Now we have
				--the number of mobs you need to kill until your rested xp runs out.
				--Then we take the total xp you need to level and substract your rested xp.
				--This is the part of your xp bar that isn't rested, and keeping in mind you
				--just gained double the xp for the mob you just killed, we need to divide
				--that mob-xp by 2, since we're not working with rested xp now. So dividing your
				--non-rested xp and the xp/2 for the mob just killed, we get the number of
				--mobs left until you level. Now we add those two numbers together, and
				--we get the total number of mobs left, having taken rested xp into account.
				
				--Easy.
				
				mobsToLevel = abs((xpRested / xp) + ((xpToLevel - xpRested) / (xp / 2))) + 1
				
			end
			
			if xpCurrent < xpTotal then 
				--Uncomment these to print variables to the chatframe for debugging
				--DEFAULT_CHAT_FRAME:AddMessage("xpTotal " .. xpTotal)
				--DEFAULT_CHAT_FRAME:AddMessage("xpRested " .. xpRested)
				--DEFAULT_CHAT_FRAME:AddMessage("xpCurrent " .. xpCurrent)
				--DEFAULT_CHAT_FRAME:AddMessage("xpToLevel " .. xpToLevel)
				--DEFAULT_CHAT_FRAME:AddMessage("mobsToLevel " .. mobsToLevel)
				--DEFAULT_CHAT_FRAME:AddMessage("creatureName " .. creatureName)

				--print our result to the chat frame
				DEFAULT_CHAT_FRAME:AddMessage(format("%d %s needed for next level.",mobsToLevel,creatureName),1,1,255)
			end
		return
	end
	end)


