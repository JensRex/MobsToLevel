## Interface: 30400
## Title: MobsTolevel
## Notes: Calculates number of mob kills needed for next level.
## Version: 1.0
## Author: Jens Rex
## X-URL: https://gitlab.com/JensRex/mobstolevel
MobsToLevel.lua
