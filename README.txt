This addon is based on another addon of the same name, the origins of which have
been lost to time. I modified it to account for rested xp and added way too many
comments. It was itself written in the early days of WoW, probably in 2005. I
dug it up again when WoW Classic was released and modified nothing at all, and
it just worked.

This addon does exactly what I want it to do, and I'm not going to add any
features to it.

For this "release" I have updated the TOC interface number info to be current as
of today, but it will break in the next patch. Either enable loading of
out-of-date addons, or change it yourself.
